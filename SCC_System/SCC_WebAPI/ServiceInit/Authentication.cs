﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class Authentication : IAuthentication
    {
        private readonly POSContext context;
        private ApiResponse response;
        public Authentication(POSContext context, ApiResponse response)
        {
            this.context = context;
            this.response = response;
        }

        public async Task<ApiResponse> AuthAccess(TblUser users)
        {
            try
            {
                if (!string.IsNullOrEmpty(users.Username) && !string.IsNullOrEmpty(users.Password))
                {
                    var result = await context.TblUser.Where(x => x.Username == users.Username && x.Password == users.Password​ && x.Active == true).FirstOrDefaultAsync();
                    if (result != null)
                    {
                        response.Data = result;
                        response.Code = 200;
                        response.Message = "success";
                    }
                    else
                    {
                        response.Code = 401;
                        response.Message = "access deny";
                    }
                }
                else
                {
                    response.Message = "input null";
                    response.Code = 401;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> GetRolePermission()
        {
            try
            {
                var list = await context.TblRolePermission.Where(a => a.Status == true).ToListAsync();
                response.Message = "success";
                response.Data = list;
                response.Code = 200;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> ListUser()
        {
            try
            {
                var user = await context.TblUser.Where(x => x.Active == true && x.EmpId > 0).ToListAsync();
                response.Code = 200;
                response.Message = "suceess";
                response.Data = user;
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }


    }
}
