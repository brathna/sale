﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class Stock : IStock
    {
        private readonly POSContext Context;
        private ApiResponse response;
        public Stock(POSContext context, ApiResponse response)
        {
            Context = context;
            this.response = response;
        }

        public async Task<ApiResponse> OutStock(TblStockOut stout)
        {
            TblStockOut stocks = new TblStockOut();
            stocks.Stid = stout.Stid;
            stocks.Customer = stout.Customer;
            stocks.Qty = stout.Qty;
            stocks.Unit = stout.Unit;
            stocks.StOutDate = stout.StOutDate;
            Context.Add(stocks);
            int result = Context.SaveChanges();
            if (result > 0)
            {
                response.Message = "success";
                response.Code = 200;
                response.Data = stocks;
            }
            return response;
        }

        public async Task<ApiResponse> CreateStock(TblStock st)
        {
            TblStock stock = new TblStock();
            stock.ProNameKh = st.ProNameKh;
            stock.ProNameEn = st.ProNameEn;
            stock.UserId = st.UserId;
            stock.Unit = st.Unit;
            stock.Qty = st.Qty;
            stock.Price = st.Price;
            stock.Amount = st.Amount;
            stock.ImportDate = st.ImportDate;
            Context.Add(stock);
            int result = Context.SaveChanges();
            if (result > 0)
            {
                response.Message = "success";
                response.Code = 200;
                response.Data = stock;
            }
            return response;
        }

        public async Task<ApiResponse> ListStock()
        {
            var data = await Context.TblStock.ToListAsync();
            response.Message = "success";
            response.Data = data;
            response.Code = 200;
            return response;
        }

        public async Task<ApiResponse> ListStockOut()
        {
            var list = await (from s in Context.TblStock
                              join o in Context.TblStockOut on s.StId equals o.Stid
                              select new
                              {
                                  s.ProNameKh,
                                  s.ProNameEn,
                                  o.Qty,
                                  o.Unit,
                                  o.Customer,
                                  o.StOutDate
                              }).ToListAsync();
            //var stockout = await Context.TblStockOut.ToListAsync();
            response.Message = "success";
            response.Data = list;
            response.Code = 200;
            return response;
        }
    }
}
