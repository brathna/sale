﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class ExchangeRate : IExchangeRate
    {
        private readonly POSContext context;
        private ApiResponse response;

        public ExchangeRate(POSContext context, ApiResponse response)
        {
            this.context = context;
            this.response = response;
        }

        public async Task<ApiResponse> CreateExchangeRate(TblExchangeRate exchange)
        {
            try
            {
                TblExchangeRate data = new TblExchangeRate();
                data.CurrencyCode = exchange.CurrencyCode;
                data.CurrencySymbol = exchange.CurrencySymbol;
                data.ExchangeDate = DateTime.Now;
                data.Rate = exchange.Rate;
                data.RateActive = true;
                //data.RateActive = exchange.RateActive; =>it is get from web insert 
                context.Add(data);
                int result = context.SaveChanges();
                if (result > 0)
                {
                    response.Code = 200;
                    response.Message = "Add successfully!";
                }
                else
                {
                    response.Message = "Add failed";
                }

            }
            catch (Exception e)
            {
                response.Code = 404;
                response.Message = e.Message;
            }
            return response;
        }

        public async Task<ApiResponse> GetExchangeRateByDate()
        {
            //var exchange = context.TblExchangeRate.OrderByDescending(rs => rs.ExchangeDate).FirstOrDefault();
            var exchange = context.TblExchangeRate.Where(a => a.RateActive == true).OrderByDescending(r => r.ExchangeDate).FirstOrDefault();
            response.Data = exchange;
            response.Code = 200;
            return response;
        }

        public async Task<ApiResponse> ListExchangeRate()
        {
            var list = context.TblExchangeRate.Where(a => a.RateActive == true).ToList();
            response.Data = list;
            response.Code = 200;
            response.Message = "Success";
            return response;
        }

        public async Task<ApiResponse> UpdateExchangeRate(TblExchangeRate exchangerate)
        {
            var data = context.TblExchangeRate.Where(rs => rs.CurrencyId.Equals(exchangerate.CurrencyId)).SingleOrDefault();
            if (data != null)
            {
                data.CurrencyCode = exchangerate.CurrencyCode;
                data.CurrencySymbol = exchangerate.CurrencySymbol;
                data.Rate = exchangerate.Rate;
                data.ExchangeDate = DateTime.Now;
                context.SaveChanges();
                response.Data = data;
                return response;
            }
            else
            {
                response.Code = 401;
                response.Message = "Bad Request";
                return response;
            }
        }

        public async Task<ApiResponse> getExchangeById(int id)
        {
            var data = context.TblExchangeRate.Where(rs => rs.CurrencyId == id).FirstOrDefault();
            if (data != null)
            {
                response.Data = data;
            }
            return response;
        }

        public async Task<ApiResponse> DeleteRate(int rateId)
        {
            if (rateId > 0)
            {
                var data = await context.TblExchangeRate.Where(r => r.CurrencyId.Equals(rateId)).SingleOrDefaultAsync();
                if (data != null)
                {
                    data.RateActive = false;
                    context.SaveChanges();
                    response.Message = "exchangeRate was deleted";
                    response.Code = 200;
                    return response;
                }
                else
                {
                    response.Message = "Data Null Can not delete";
                    response.Code = 401;
                    return response;
                }
            }
            else
            {
                response.Message = "Id must greater than zero";
                response.Code = 401;
                return response;
            }
        }
    }
}
