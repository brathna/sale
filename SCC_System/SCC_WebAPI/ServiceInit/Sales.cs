﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class Sales : ISales
    {
        private readonly POSContext Context;
        private ApiResponse response;
        public Sales(POSContext context, ApiResponse responses)
        {
            this.Context = context;
            this.response = responses;
        }

        public async Task<ApiResponse> CreateSale(SalePost post)
        {
            try
            {
                if (post != null && post.details != null)
                {
                    TblOrder order = new TblOrder();
                    order.OrderDate = post.OrderDate;
                    order.UserId = post.UserId;
                    order.GrandTotal = post.GrandTotal;
                    order.CashRecieve = post.CashRecieve;
                    order.PayBack = post.PayBack;
                    order.SubTotal = post.SubTotal;
                    order.DisId = post.DisId;
                    order.Amount = post.Amount;
                    order.CurrencyId = post.CurrencyId;
                    order.Rate = post.Rate;
                    this.Context.TblOrder.Add(order);
                    await this.Context.SaveChangesAsync();
                    int saleId = this.Context.TblOrder.Max(x => x.OrderId);
                    if (post.details.Count > 0)
                    {
                        foreach (var item in post.details)
                        {
                            TblOrderDetail orderDetail = new TblOrderDetail();
                            orderDetail.OrderId = saleId;
                            orderDetail.ProId = item.ProId;
                            orderDetail.Qty = item.Qty;
                            orderDetail.ProNameEn = item.ProNameEn;
                            orderDetail.ProNameKh = item.ProNameKh;
                            orderDetail.Price = item.Price;
                            orderDetail.Size = item.Size;
                            Context.TblOrderDetail.Add(orderDetail);
                            Context.SaveChanges();
                        }
                        response.Code = 200;
                        response.Message = "success";
                    }
                    else
                    {
                        response.Code = 401;
                        response.Message = "bad request";
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                response.Data = null;
                return response;
            }
        }

        public async Task<ApiResponse> GetListDiscount()
        {
            try
            {
                var data = await this.Context.TblDiscount.Where(a => a.Status == true).ToListAsync();
                this.response.Data = data;
                this.response.Code = 200;
                this.response.Message = "success";
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                response.Data = null;
                return response;
            }
        }

        public async Task<ApiResponse> GetListExchangeRate()
        {
            //var exchange = await Context.TblExchangeRate.Where(a => a.CurrencyId.Equals(a.CurrencyId)).ToListAsync();
            var exchange = await Context.TblExchangeRate.Where(a => a.RateActive == true).OrderByDescending(r => r.ExchangeDate).ToListAsync();
            response.Message = "success";
            response.Code = 200;
            response.Data = exchange;
            return response;
        }

        public async Task<ApiResponse> GetListProduct()
        {
            try
            {
                var result = await this.Context.TblProducts.ToListAsync();
                this.response.Data = result;
                this.response.Code = 200;
                this.response.Message = "success";

                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                response.Data = null;
                return response;
            }
        }

        public async Task<ApiResponse> GetOrderDetail()
        {
            var tmpOrder = Context.TblOrder.OrderByDescending(rs => rs.OrderId).FirstOrDefault();

            var list = await (from o in Context.TblOrder
                              join od in Context.TblOrderDetail on o.OrderId equals od.OrderId
                              where o.OrderId == tmpOrder.OrderId
                              select new
                              {
                                  o.OrderId,
                                  o.UserId,
                                  o.OrderDate,
                                  o.GrandTotal,
                                  o.CashRecieve,
                                  o.PayBack,
                                  o.SubTotal,
                                  o.Amount,
                                  o.Rate,

                                  od.OrderDetailId,
                                  od.ProId,
                                  od.ProNameEn,
                                  od.ProNameKh,
                                  od.Qty,
                                  od.Price,
                                  od.Size,
                              }).OrderByDescending(x => x.OrderId).ToListAsync();

            //var tmp = Context.TblOrder.OrderByDescending(rs => rs.OrderId).Select(rs => rs.OrderId).FirstOrDefault();
            //var list = await Context.TblOrderDetail.Where(x => x.OrderId == tmp).ToListAsync();
            //var list = await Context.VwGetOrderById.OrderByDescending(x => x.OrderId).FirstOrDefaultAsync();
            response.Data = list;
            response.Code = 200;
            response.Message = "Success";
            return response;
        }

        public async Task<ApiResponse> GetSaleDetail(int saleId)
        {
            try
            {
                if (saleId > 0)
                {
                    var sale = await Context.VwGetOrderById.Where(x => x.OrderId == saleId).FirstOrDefaultAsync();
                    if (sale != null)
                    {
                        response.Code = 200;
                        response.Data = sale;
                        response.Message = "success";
                    }
                    else
                    {
                        response.Code = 404;
                        response.Data = null;
                        response.Message = "data not found";
                    }
                }
                else
                {
                    response.Code = 401;
                    response.Data = null;
                    response.Message = "bad request";
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                response.Data = null;
                return response;
            }

        }

        public async Task<ApiResponse> ListSales()
        {
            try
            {
                var sale = await Context.VwGetOrderById.ToListAsync();
                response.Code = 200;
                response.Data = sale;
                response.Message = "success";

                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                response.Data = null;
                return response;
            }
        }

    }
}
