﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class Import : IImport
    {
        private readonly POSContext context;
        private ApiResponse response;
        public Import(POSContext context, ApiResponse response)
        {
            this.context = context;
            this.response = response;
        }

        public async Task<ApiResponse> CreateImport(ImportPost importPost)
        {
            try
            {
                if (importPost != null && importPost.details != null)
                {
                    TblImport data = new TblImport();
                    data.UserId = importPost.UserId;
                    data.ImpDate = importPost.ImpDate;
                    data.ImpInsertDate = importPost.ImpInsertDate;
                    this.context.TblImport.Add(data);
                    await this.context.SaveChangesAsync();

                    int importId = this.context.TblImport.Max(x => x.ImpId);

                    if (importPost.details.Count > 0)
                    {
                        foreach (var item in importPost.details)
                        {
                            TblImportDetail importDetail = new TblImportDetail();
                            importDetail.ImpId = importId;
                            importDetail.Qty = item.Qty;
                            importDetail.PriceImport = item.PriceImport;
                            importDetail.Unit = item.Unit;
                            importDetail.ProNameKh = item.ProNameKh;
                            importDetail.ProNameEn = item.ProNameEn;

                            context.TblImportDetail.Add(importDetail);
                            context.SaveChanges();
                        }
                        response.Code = 200;
                        response.Message = "success";
                        response.Data = importPost;
                    }
                    else
                    {
                        response.Code = 401;
                        response.Message = "bad request";
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                response.Data = null;
                return response;
            }
        }

        public async Task<ApiResponse> GetImportDetail(int importId)
        {
            try
            {
                if (importId > 0)
                {
                    var import = await context.VwGetImportById.Where(x => x.ImpId == importId).SingleOrDefaultAsync();
                    if (import != null)
                    {
                        response.Code = 200;
                        response.Data = import;
                        response.Message = "success";
                    }
                    else
                    {
                        response.Code = 404;
                        response.Data = null;
                        response.Message = "data not found";
                    }
                }
                else
                {
                    response.Code = 401;
                    response.Data = null;
                    response.Message = "bad request";
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                response.Data = null;
                return response;
            }
        }

        public async Task<ApiResponse> ListImport()
        {
            try
            {
                var list = await context.VwGetImportById.ToListAsync();
                response.Code = 200;
                response.Data = list;
                response.Message = "success";
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                response.Data = null;
                return response;
            }
        }
    }
}
