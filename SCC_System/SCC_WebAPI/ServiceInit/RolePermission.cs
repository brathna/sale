﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class RolePermission : IRolePermission
    {
        private readonly POSContext Context;
        private ApiResponse response;
        public RolePermission(POSContext context, ApiResponse response)
        {
            Context = context;
            this.response = response;
        }

        public async Task<ApiResponse> CreateRolePermission(TblRolePermission roleper)
        {
            try
            {
                if (roleper.RoleId > 0 && roleper.PerId > 0)
                {
                    Context.TblRolePermission.Add(roleper);
                    await Context.SaveChangesAsync();
                    response.Message = "Sucess";
                    response.Code = 200;
                    response.Data = roleper;
                }
                else
                {
                    response.Message = "Data can not null or zero";
                    response.Code = 401;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> DeleteRolePermission(int id)
        {
            try
            {
                if (id > 0)
                {
                    var role = await Context.TblRolePermission.Where(x => x.RolePerId.Equals(id)).SingleOrDefaultAsync();
                    if (role != null)
                    {
                        role.Status = false;
                        await Context.SaveChangesAsync();
                        response.Message = "Success";
                        response.Code = 200;
                        return response;
                    }
                    else
                    {
                        response.Message = "Bad Request";
                        response.Code = 401;
                        return response;
                    }
                }
                else
                {
                    response.Code = 401;
                    response.Message = "Bad Request";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> GetPermByList()
        {
            try
            {
                var per = await Context.TblPermission.Where(x => x.Status == true).ToListAsync();
                response.Message = "Success";
                response.Code = 200;
                response.Data = per;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> GetRoleByList()
        {
            try
            {
                var roles = await Context.TblRole.Where(x => x.Status == true).ToListAsync();
                response.Message = "Sucess";
                response.Code = 200;
                response.Data = roles;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> GetRolePerById(int id)
        {
            try
            {
                var result = await Context.TblRolePermission.Where(x => x.RolePerId.Equals(id)).SingleOrDefaultAsync();
                if (result != null)
                {
                    response.Data = result;
                    response.Code = 200;
                    response.Message = "Get Success";
                    return response;
                }
                else
                {
                    response.Message = "Failed Request";
                    response.Code = 401;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> GetSubPermissionList()
        {
            try
            {
                var data = await Context.TblSubPermission.Where(x => x.Active == true).ToListAsync();
                response.Message = "Sucess";
                response.Code = 200;
                response.Data = data;
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> ListRolePermission()
        {
            try
            {
                var data = await Context.VwGetRolePermission.Where(x => x.Status == true).ToListAsync();
                response.Message = "Sucess";
                response.Code = 200;
                response.Data = data;
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> UpdateRolePermission(TblRolePermission rolePermission)
        {
            try
            {
                if (rolePermission != null && rolePermission.RolePerId > 0 && rolePermission.RoleId > 0 && rolePermission.PerId > 0 && rolePermission.Status == true)
                {
                    var roleper = await Context.TblRolePermission.Where(x => x.RolePerId.Equals(rolePermission.RolePerId)).SingleOrDefaultAsync();
                    if (roleper != null)
                    {
                        roleper.RolePerId = rolePermission.RolePerId;
                        roleper.RoleId = rolePermission.RoleId;
                        roleper.PerId = rolePermission.PerId;

                        await Context.SaveChangesAsync();
                        response.Message = "Sucess";
                        response.Code = 200;
                        response.Data = roleper;
                        return response;
                    }
                    else
                    {
                        response.Code = 401;
                        response.Message = "Bad Request";
                        return response;
                    }
                }
                else
                {
                    response.Code = 401;
                    response.Message = "Data not much with table";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

    }
}
