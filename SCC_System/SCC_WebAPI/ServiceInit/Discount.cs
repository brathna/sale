﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class Discount : IDiscount
    {
        private readonly POSContext Context;
        private ApiResponse response;

        public Discount(POSContext context, ApiResponse response)
        {
            this.Context = context;
            this.response = response;
        }

        public async Task<ApiResponse> CreateDiscount(TblDiscount cre_dis)
        {
            try
            {
                TblDiscount disco = new TblDiscount();
                disco.Amount = cre_dis.Amount;
                disco.DisPercent = cre_dis.DisPercent;
                disco.DisStartDate = DateTime.Now;
                disco.DisEndDate = cre_dis.DisEndDate;
                disco.Status = cre_dis.Status;
                Context.Add(disco);
                int result = Context.SaveChanges();
                if (result > 0)
                {
                    response.Data = disco;
                    response.Message = "Add successfully!";
                }
                else
                {
                    response.Message = "Add failed";
                }

            }
            catch (Exception e)
            {
                response.Code = 404;
                response.Message = e.Message;
            }
            return response;
        }

        public async Task<ApiResponse> DeleteDiscount(int de_disId)
        {
            if (de_disId > 0)
            {
                var data = await Context.TblDiscount.Where(r => r.DisId.Equals(de_disId)).SingleOrDefaultAsync();
                if (data != null)
                {
                    data.Status = false;
                    Context.SaveChanges();
                    response.Message = "Discount was deleted";
                    response.Code = 200;
                    return response;
                }
                else
                {
                    response.Message = "Data Null Can not delete";
                    response.Code = 401;
                    return response;
                }
            }
            else
            {
                response.Message = "Id must greater than zero";
                response.Code = 401;
                return response;
            }
        }

        public async Task<ApiResponse> GetDiscountById(int disId)
        {
            var data = await Context.TblDiscount.Where(f => f.DisId == disId).FirstOrDefaultAsync();
            if (data != null)
            {
                response.Data = data;
                response.Message = "Get by ID successfully!";
                response.Code = 200;
            }
            return response;
        }

        public async Task<ApiResponse> ListDiscount()
        {
            try
            {
                //var list = await Context.TblDiscount.ToListAsync();
                var list = await Context.TblDiscount.Where(a => a.Status == true).ToListAsync();
                response.Data = list;
                response.Code = 200;
                response.Message = "sucess";
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> UpdateDiscount(TblDiscount up_dis)
        {
            var result = await Context.TblDiscount.Where(r => r.DisId.Equals(up_dis.DisId)).SingleOrDefaultAsync();
            if (result != null)
            {
                result.Amount = up_dis.Amount;
                result.DisPercent = up_dis.DisPercent;
                result.DisStartDate = up_dis.DisStartDate;
                result.DisEndDate = up_dis.DisEndDate;
                //if (up_dis.DisEndDate == DateTime.Now)
                //{
                //    result.Status = false;
                //}
                result.Status = up_dis.Status;
                Context.SaveChanges();
                response.Data = result;
                response.Message = "Update Success";
                response.Code = 200;
                return response;
            }
            else
            {
                response.Code = 401;
                response.Message = "Bad Request";
                return response;
            }
        }
    }
}
