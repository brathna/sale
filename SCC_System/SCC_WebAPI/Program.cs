using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.Services;
using SCC_WebAPI.ServiceInit;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
//builder.Services.AddDbContext<AppContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
var connectionString = builder.Configuration.GetConnectionString("Db_connection");
builder.Services.AddDbContext<POSContext>(x =>
{
    x.UseSqlServer(connectionString);
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//advetize all class in salemodal that implement new here!
builder.Services.AddScoped<IEmployee, Employee>();
builder.Services.AddScoped<IProduct, Product>();
builder.Services.AddScoped<ISales, Sales>();
builder.Services.AddScoped<ICategory, Category>();
builder.Services.AddScoped<IUser, User>();
builder.Services.AddScoped<IRolePermission, RolePermission>();
builder.Services.AddScoped<ISaleReport, SaleReport>();
builder.Services.AddScoped<IExchangeRate, ExchangeRate>();
builder.Services.AddScoped<IImport, Import>();
builder.Services.AddScoped<IAuthentication, Authentication>();
builder.Services.AddScoped<IDiscount, Discount>();
builder.Services.AddScoped<IStock, Stock>();


builder.Services.AddScoped<ApiResponse>();
builder.Services.AddScoped<SalePost>();
builder.Services.AddScoped<SaleDetail>();
builder.Services.AddScoped<SaleFilter>();
builder.Services.AddScoped<ImportPost>();



string BaseAddress = "BaseAddress";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: BaseAddress, builder =>
    {
        builder.AllowAnyOrigin();
        builder.AllowAnyHeader();
        builder.AllowAnyMethod();
    });
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
//======addbasea address========
app.UseCors(BaseAddress);

app.UseAuthorization();

app.MapControllers();

app.Run();
