﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RolePermissionController : Controller
    {
        private readonly IRolePermission rolePermission;
        public RolePermissionController(IRolePermission rolePermission)
        {
            this.rolePermission = rolePermission;
        }
        [HttpGet("ListRolePermission")]
        public async Task<ApiResponse> ListRolePermission()
        {
            return await rolePermission.ListRolePermission();
        }

        [HttpGet("GetSubPermissionList")]
        public async Task<ApiResponse> GetSubPermissionList()
        {
            return await rolePermission.GetSubPermissionList();
        }

        [HttpGet("GetPermissionList")]
        public async Task<ApiResponse> GetPermissionList()
        {
            return await rolePermission.GetPermByList();
        }

        [HttpPost("CreateRolePermission")]
        public async Task<ApiResponse> CreateRolePer(TblRolePermission roleper)
        {
            return await rolePermission.CreateRolePermission(roleper);
        }

        [HttpPost("UpdateRolePermission")]
        public async Task<ApiResponse> UpdateRolePermission(TblRolePermission role)
        {
            return await rolePermission.UpdateRolePermission(role);
        }

        [HttpGet("DeleteRolePermission/{id:int}")]
        public async Task<ApiResponse> DeleteRolePer(int id)
        {
            return await rolePermission.DeleteRolePermission(id);
        }
        [HttpGet("GetRolePerById/{id:int}")]
        public async Task<ApiResponse> GetRolePerById(int id)
        {
            return await rolePermission.GetRolePerById(id);
        }
    }
}
