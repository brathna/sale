﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StockController : Controller
    {
        private readonly IStock stock;
        public StockController(IStock stock)
        {
            this.stock = stock;
        }
        [HttpGet("ListStock")]
        public async Task<ApiResponse> ListStock()
        {
            return await stock.ListStock();
        }

        [HttpGet("ListStockOut")]
        public async Task<ApiResponse> ListStockOut()
        {
            return await stock.ListStockOut();
        }

        [HttpPost("CreateStock")]
        public async Task<ApiResponse> CreateStock(TblStock sto)
        {
            return await this.stock.CreateStock(sto);
        }

        [HttpPost("OutStock")]
        public async Task<ApiResponse> OutStock(TblStockOut stout)
        {
            return await this.stock.OutStock(stout);
        }

    }
}
