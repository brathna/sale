﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers

{ 
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeeController : Controller
    {

        private readonly IEmployee emp;
        //constructor auto generate ctor
        public EmployeeController(IEmployee emp)
        {
            this.emp = emp;
        }

        [HttpGet("Listemployee")]
         public async Task<ApiResponse> Listemployee()
        {
            return await emp.ListEmployee();
        }

        [HttpGet("GetEmployeeById/{id:int}")]
         public async Task<ApiResponse> GetEmployeeById(int id)
        {
            return await emp.GetEmployeeById(id);
        }


        [HttpPost ("CreateEmployee")]
        public async Task<ApiResponse> CreateEmployee(TblEmployee employee)
        {
            return await emp.CreateEmployee(employee);
        }


        [HttpPost("UpdateEmployee")]
        public async Task<ApiResponse> UpdateEmployee(TblEmployee employee)
        {
            return await emp.UpdateEmployee(employee);
        }


        [HttpGet("DeleteEmployee/{id:int}")]
        public async Task<ApiResponse> DeleteEmployee(int id)
        {
            return await emp.DeleteEmployee(id);
        }

    }
}
