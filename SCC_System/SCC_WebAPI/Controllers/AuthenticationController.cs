﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class AuthenticationController : Controller
    {
        private readonly IAuthentication authentication;
        public AuthenticationController(IAuthentication authentication)
        {
            this.authentication = authentication;
        }
        [HttpGet("UserList")]
        public async Task<ApiResponse> UseList()
        {
            return await authentication.ListUser();
        }
        [HttpGet("RolePermission")]
        public async Task<ApiResponse> RolePermission()
        {
            return await authentication.GetRolePermission();
        }
        [HttpPost("AuthAccess")]
        public async Task<ApiResponse> AuthAccess(TblUser user)
        {
            return await authentication.AuthAccess(user);
        }
    }
}
