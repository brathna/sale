﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExchangeRateController : ControllerBase
    {
        private readonly IExchangeRate exchangeRate;

        public ExchangeRateController(IExchangeRate exchangeRate)
        {
            this.exchangeRate = exchangeRate;
        }

        [HttpGet("ListExchangeRate")]
        public async Task<ApiResponse> ListExchangeRate()
        {
            return await exchangeRate.ListExchangeRate();
        }

        [HttpPost("CreateExchangeRate")]
        public async Task<ApiResponse> CreateExchangeRate(TblExchangeRate data)
        {
            return await exchangeRate.CreateExchangeRate(data);
        }

        [HttpGet("search_date")]
        public async Task<ApiResponse> GetExchangeRateByDate()
        {
            return await exchangeRate.GetExchangeRateByDate();
        }

        [HttpPost("UpdateExchangeRate")]
        public async Task<ApiResponse> UpdateExchangeRate(TblExchangeRate data)
        {
            return await exchangeRate.UpdateExchangeRate(data);
        }

        [HttpGet("id")]
        public async Task<ApiResponse> getById(int id)
        {
            return await exchangeRate.getExchangeById(id);
        }
        [HttpGet("DeleteExchangeRate")]
        public async Task<ApiResponse> DeleteDiscount(int id)
        {
            return await exchangeRate.DeleteRate(id);
        }

    }
}
