﻿using SaleModal.Classes;

namespace SaleModal.Services
{
    public interface IImport
    {
        public Task<ApiResponse> ListImport();
        public Task<ApiResponse> CreateImport(ImportPost importPost);
        public Task<ApiResponse> GetImportDetail(int importId);

    }
}
