﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaleModal.Classes;
using SaleModal.DB.Model;
namespace SaleModal.Services
{
    public  interface IEmployee
    {
        public Task<ApiResponse> ListEmployee();
        public Task<ApiResponse> CreateEmployee(TblEmployee emp);
        public Task<ApiResponse> DeleteEmployee(int id);
        public Task<ApiResponse> UpdateEmployee(TblEmployee employee);
        public Task<ApiResponse> GetEmployeeById(int id); 
    }
}
