﻿using SaleModal.Classes;
using SaleModal.DB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaleModal.Services
{
    public interface ICategory
    {
        public Task<ApiResponse> ListCategory();
        public Task<ApiResponse> CreateCategory(TblCategory cate);
        public Task<ApiResponse> UpdateCategory(TblCategory category);
        public Task<ApiResponse> GetCategoryById(int id);
        public Task<ApiResponse> DeleteCategory(int id);
       
    }
}
