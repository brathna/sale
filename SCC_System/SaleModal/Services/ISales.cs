﻿using SaleModal.Classes;

namespace SaleModal.Services
{
    public interface ISales
    {
        public Task<ApiResponse> ListSales();
        public Task<ApiResponse> CreateSale(SalePost post);
        public Task<ApiResponse> GetSaleDetail(int saleId);
        public Task<ApiResponse> GetListProduct();
        public Task<ApiResponse> GetListDiscount();
        public Task<ApiResponse> GetListExchangeRate();
        public Task<ApiResponse> GetOrderDetail();
    }
}
