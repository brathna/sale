﻿using SaleModal.Classes;
using SaleModal.DB.Model;

namespace SaleModal.Services
{
    public interface IAuthentication
    {
        public Task<ApiResponse> AuthAccess(TblUser users);
        public Task<ApiResponse> ListUser();
        public Task<ApiResponse> GetRolePermission();
    }
}
