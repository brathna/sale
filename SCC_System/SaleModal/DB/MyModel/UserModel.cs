﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaleModal.DB.MyModel
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string EmpId { get; set; }
        public string Email { get; set; }
        public string? RoleId { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public DateTime? DateCreate { get; set; } = DateTime.Now;
        public bool? Active { get; set; }
    }
}
