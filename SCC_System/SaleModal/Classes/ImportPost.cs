﻿using SaleModal.DB.Model;

namespace SaleModal.Classes
{
    public class ImportPost
    {
        public int? UserId { get; set; }
        public DateTime? ImpDate { get; set; }
        public DateTime? ImpInsertDate { get; set; } = DateTime.Now;
        public List<TblImportDetail> details { get; set; }
    }
}
