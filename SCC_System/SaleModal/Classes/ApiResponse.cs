﻿namespace SaleModal.Classes
{
    public class ApiResponse
    {
        public string? Message { get; set; }
        public int Code { get; set; }
        public Object? Data { get; set; }

    }
}
