﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;
namespace SCC_System.Controllers.Sales
{
    public class SaleBase : ComponentBase
    {
        [Inject]
        Radzen.NotificationService NotificationService { get; set; }
        [Inject]
        private IJSRuntime Runtime { get; set; }
        [Inject]
        public SaleResponse Sale { get; set; }
        [Inject]
        public ExchangeResponse Exchange { get; set; }
        //[Inject]
        //public DiscountResponse discount { get; set; }
        [Inject]
        NavigationManager navigate { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        public CategoryResponse Category { get; set; }
        public TblExchangeRate ExchangeRate = new TblExchangeRate();
        public List<TblExchangeRate> listExchangeRate = new List<TblExchangeRate>();
        public List<TblDiscount> listDisc = new List<TblDiscount>();
        public List<VwSelectAllProduct> ListProduct = new List<VwSelectAllProduct>();
        public List<TblOrderDetail> OrderDetails = new List<TblOrderDetail>();
        public List<TblCategory> Categories = new List<TblCategory>();
        public SalePost salePost = new SalePost();
        public string name = "";

        public decimal GrandTotal { get; set; }
        public decimal CashRecieve { get; set; }
        public decimal PayBack { get; set; }
        public decimal Total { get; set; }
        public int Disid { get; set; }
        public decimal Amount = 0;
        //public decimal Discount { get; set; }
        public decimal SubTotal { get; set; }
        public int CurrencyId { get; set; }
        public decimal Rate { get; set; }
        //public string Size { get; set; }
        public string size = "";
        public string S, M, L = "";
        //public bool isStateChange { get; set; } = false;
        public int index { get; set; }
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigate.NavigateTo("/login");
            }
            this.ListProduct = await this.Sale.GetProducts();
            this.Categories = await this.Category.CategoryLists();
            this.ExchangeRate = await this.Exchange.GetExchangeRateByDate();
            this.listExchangeRate = await this.Sale.GetExchangRate();
            this.listDisc = await this.Sale.GetListDiscount();
            foreach (var item in listDisc)
            {
                if (item.Status == true && item.Amount > 0)
                {
                    Disid = item.DisId;
                    Amount = Convert.ToDecimal(item.Amount);
                }
            }
            foreach (var item in listExchangeRate)
            {
                if (item.RateActive == true)
                {
                    CurrencyId = item.CurrencyId;
                    Rate = Convert.ToDecimal(item.Rate);
                }
            }
            StateHasChanged();
        }

        public async void OnClickCate(int CateId)
        {
            var list = await this.Sale.GetProducts();
            ListProduct = list.Where(f => f.CateId == CateId).ToList();
            StateHasChanged();
        }

        public async Task OnRemove(TblOrderDetail orderDetail)
        {
            if (orderDetail.Qty > 1)
            {
                orderDetail.Qty = orderDetail.Qty - 1;
                //salePost.GrandTotal = salePost.GrandTotal - float.Parse(orderDetail.Price.Value.ToString());
                salePost.GrandTotal = salePost.GrandTotal - orderDetail.Price;
                GrandTotal = GrandTotal - Convert.ToDecimal(orderDetail.Price);
                SubTotal = GrandTotal * (100 - Amount) / 100;
                //SubTotal = SubTotal - Convert.ToDecimal(orderDetail.Price);
                //SubTotal = SubTotal - Convert.ToDecimal(orderDetail.Price) + Amount/100;   //=> remove discount
                OrderDetails = OrderDetails.Where(f => f.ProId != orderDetail.ProId).ToList();          //remove item from orderDetail
                OrderDetails.Add(orderDetail);
                OrderDetails = OrderDetails.ToList();
                StateHasChanged();
            }
            else
            {
                salePost.GrandTotal = salePost.GrandTotal - orderDetail.Price;
                GrandTotal = GrandTotal - Convert.ToDecimal(orderDetail.Price);
                SubTotal = GrandTotal * (100 - Amount) / 100;
                //SubTotal = 0;
                //SubTotal = SubTotal - Convert.ToDecimal(orderDetail.Price);    //=> remove discount
                OrderDetails.Remove(orderDetail);
                OrderDetails = OrderDetails.ToList();
                StateHasChanged();
            }

        }

        public async Task OnSaveOrder()
        {
            if (salePost != null)
            {
                if (salePost.details == null)
                {
                    await Runtime.InvokeAsync<object>("warning", "សូមបញ្ចូលទំនិញមុនធ្វើការទូទាត់!");
                }
                else if (salePost.CashRecieve == 0)
                {
                    await Runtime.InvokeAsync<object>("warning", "សូមបញ្ចូលប្រាក់ទទួល!");
                }
                else if (salePost.CashRecieve < GrandTotal && SubTotal == GrandTotal && Amount == 0)
                {
                    await Runtime.InvokeAsync<object>("warning", "ទឹកប្រាក់មិនអាចតូចជាងតម្លៃសរុប​!");
                }
                else if (salePost.CashRecieve < SubTotal && Amount > 0)
                {
                    await Runtime.InvokeAsync<object>("warning", "ទឹកប្រាក់មិនអាចតូចជាងតម្លៃបញ្ចុះរួចទេ!​!");
                }
                else
                {
                    string name = await localStorage.GetItemAsync<string>("user");
                    salePost.UserId = int.Parse(name);
                    salePost.SubTotal = SubTotal;
                    salePost.Amount = Amount;
                    salePost.Size = size;
                    salePost.DisId = Disid;
                    salePost.CurrencyId = CurrencyId;

                    salePost.Rate = Rate;
                    var result = await Sale.AddSale(salePost);
                    if (result.Code == 200)
                    {
                        GrandTotal = 0;
                        salePost = new SalePost();
                        OrderDetails = new List<TblOrderDetail>();
                        Total = 0;
                        CashRecieve = 0;
                        PayBack = 0;
                        SubTotal = 0;
                        Amount = 0;
                        StateHasChanged();
                        await Runtime.InvokeAsync<object>("warning", "ប្រតិបត្តិការជោគជ័យ​!");
                        navigate.NavigateTo("/invoice");
                    }
                    else
                    {
                        await Runtime.InvokeAsync<object>("warning", result.Message);
                    }

                }
            }
        }

        public async Task OnCancelOrder()
        {
            GrandTotal = 0;
            salePost = new SalePost();
            OrderDetails = new List<TblOrderDetail>();
            Total = 0;
            CashRecieve = 0;
            PayBack = 0;
            SubTotal = 0;
            size = "";
            Disid = 0;
            CurrencyId = 0;
            StateHasChanged();
        }

        public async Task SelectSize(string sizeclick)
        {
            this.size = sizeclick;
        }

        public async Task ProductClick(VwSelectAllProduct pro)
        {
            if (this.size == "")
            {
                Message.Error(NotificationService, "សូមជ្រើសរើសទំហំភេសជ្ជៈ");
            }
            else
            {
                index = 1;
                GrandTotal += Convert.ToDecimal(pro.Price​);
                if (size == "M")
                {
                    GrandTotal += 2000;
                }
                else if (size == "L")
                {
                    GrandTotal += 4000;
                }
                SubTotal = GrandTotal * (100 - Amount) / 100;
                if (Amount == 0) //amount 
                {
                    SubTotal = GrandTotal;
                }
                //await SelectSize(size);
                TblOrderDetail detail = new TblOrderDetail();
                detail.ProId = pro.ProId;
                detail.ProNameKh = pro.ProductNameKh;
                detail.ProNameEn = pro.ProductNameEn;
                detail.Size = this.size;
                detail.Price = this.size == "S" ? pro.Price : (this.size == "M" ? pro.Price + 2000 : pro.Price + 4000);
                detail.Qty = 1;

                var result = OrderDetails.Where(x => x.ProId == pro.ProId).FirstOrDefault();

                if (result != null && result.ProId == pro.ProId && result.Size.Equals(this.size))
                {
                    result.Qty++;
                }
                else
                {
                    OrderDetails.Add(detail);
                    OrderDetails = OrderDetails.ToList();
                }
                salePost.GrandTotal = GrandTotal;
                salePost.CashRecieve = CashRecieve;
                salePost.PayBack = PayBack;
                salePost.SubTotal = SubTotal;
                salePost.OrderDate = DateTime.Now.Date;
                salePost.details = OrderDetails;
                salePost.Size = size;
                this.size = "";
                StateHasChanged();

            }
        }

        public async Task CashRecieveChange(ChangeEventArgs e)
        {
            if (e.Value != null && !string.IsNullOrEmpty(e.Value.ToString()))
            {
                decimal cash = decimal.Parse(e.Value.ToString());
                CashRecieve = cash;
                this.salePost.CashRecieve = CashRecieve;
                if (GrandTotal == 0)
                {
                    await Runtime.InvokeAsync<object>("warning", "សូមបញ្ចូលទំនិញមុនធ្វើការទូរទាត់!");
                }
                else if (CashRecieve > GrandTotal && SubTotal > 0 && Amount == 0)
                {
                    PayBack = CashRecieve - GrandTotal;
                    Message.Success(NotificationService, "សូមធ្វើការទូទាត់");
                }
                else if (CashRecieve > SubTotal && SubTotal > 0 && Amount > 0)
                {
                    PayBack = CashRecieve - SubTotal;
                    Message.Success(NotificationService, "ទទួលបានការបញ្ចុះតម្លៃ សូមធ្វើការទូទាត់!");
                }
                else if (CashRecieve < GrandTotal && CashRecieve < SubTotal)
                {
                    Message.Error(NotificationService, "ទឹកប្រាក់មិនគ្រប់!");
                }
                else
                {
                    PayBack = 0;
                    Message.Success(NotificationService, "សូមធ្វើការទូទាត់");
                }

            }
            else
            {
                this.salePost.CashRecieve = 0;
                PayBack = 0;
            }

            this.salePost.PayBack = PayBack;
            index = 1;
        }


    }
}
