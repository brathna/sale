﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Category
{
    public class EditCategoryBase : ComponentBase
    {
        [Parameter]
        public string? categoryId { get; set; }
        [Inject]
        private NavigationManager? Navigation { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }

        [Inject]
        public CategoryResponse? CategoryResponse { get; set; }
        public TblCategory? UpCategory { get; set; }
        public string name = "";

        protected async override Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                Navigation.NavigateTo("/login");
            }

            if (categoryId != null)
            {
                int id = int.Parse(categoryId);
                UpCategory = await CategoryResponse.GetCategoryById(id);
            }
        }

        public async Task SaveCategory()
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែមែនទេ?");
            if (result)
            {
                if (UpCategory != null && categoryId != null)
                {
                    if (!string.IsNullOrEmpty(UpCategory.CategoryNameEn) && !string.IsNullOrEmpty(UpCategory.CategoryNameKh))
                    {
                        await CategoryResponse.EditCategory(UpCategory);
                        Message.Success(NotificationService, "ជោគជ័យ");
                        StateHasChanged();
                        Navigation.NavigateTo("/categorylist");
                    }
                    else
                    {
                        Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");
                    }
                }
            }

        }

        public void onBtnExitCategory()
        {
            Navigation.NavigateTo("/categorylist");
        }
    }
}
