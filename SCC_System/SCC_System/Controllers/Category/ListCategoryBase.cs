﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.Category
{
    public class ListCategoryBase : ComponentBase
    {
        [Inject]
        public CategoryResponse? CategoryResponse { get; set; }
        public List<TblCategory>? CategoryList { get; set; }
        public string name = "";

        [Inject]
        NavigationManager navigation { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }


        protected async override Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }
            CategoryList = await CategoryResponse.CategoryLists();
        }

        public async void onBtnRefresh()
        {
            CategoryList = await CategoryResponse.CategoryLists();
        }
        public void onBtnNewCategory()
        {
            navigation.NavigateTo("/newcategory");
        }
        public async Task DeleteCategory(int id)
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់លុបមែនទេ?");
            if (result)
            {
                await CategoryResponse.DeleteCategory(id);
                CategoryList = await CategoryResponse.CategoryLists();
                StateHasChanged();
            }

        }
    }
}
