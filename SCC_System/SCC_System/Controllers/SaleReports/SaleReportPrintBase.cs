﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;

namespace SCC_System.Controllers.SaleReports
{
    public class SaleReportPrintBase : ComponentBase
    {
        public List<sp_SaleReportResult> salereportresult { get; set; }
        public string name = "";
        [Inject]
        NavigationManager navigate { get; set; }
        [Inject]
        public IJSRuntime JS { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigate.NavigateTo("/login");
            }

            salereportresult = new List<sp_SaleReportResult>();
            StateHasChanged();
        }


        public void OnPrint(List<sp_SaleReportResult> salereportresult)
        {
            this.salereportresult = salereportresult.ToList();
            JS.InvokeVoidAsync("PrintDiv", "report");
            StateHasChanged();
        }


    }
}
