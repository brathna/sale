﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.User
{
    public class EditUserBase : ComponentBase
    {
        [Parameter]
        public string? userId { get; set; }
        [Inject]
        public UserResponse? ur { get; set; }
        public TblUser? User { get; set; }
        public List<TblRole> Roleslists { get; set; }
        public List<TblEmployee> listemployee = new List<TblEmployee>();
        public string name = "";
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        NavigationManager Navigation { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                Navigation.NavigateTo("/login");
            }

            Roleslists = new List<TblRole>();
            if (userId != null)
            {
                int id = int.Parse(userId);
                User = await ur.GetUserById(id);
                Roleslists = await ur.GetRoleByList();
                this.listemployee = await ur.GetEmployeeList();
                StateHasChanged();
            }
        }


        public async Task SaveUser()
        {
            if (User != null && ur != null)
            {
                if (!string.IsNullOrEmpty(User.Username) && !string.IsNullOrEmpty(User.Password) && User.EmpId > 0 && User.RoleId > 0)
                {
                    bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែមែនទេ?");
                    if (result)
                    {
                        await ur.EditUser(User);
                        Message.Success(NotificationService, "ជោគជ័យ");
                        StateHasChanged();
                        Navigation.NavigateTo("/account");
                    }
                }
                else
                {
                    Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");
                }
            }
        }

        public void onBtnExitUser()
        {
            Navigation.NavigateTo("/account");
        }
    }
}
