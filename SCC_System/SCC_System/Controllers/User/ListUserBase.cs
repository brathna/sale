﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.User
{
    public class ListUserBase : ComponentBase
    {
        [Inject]
        NavigationManager Navigate { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        public UserResponse? ur { get; set; }
        public List<VwGetUsers>? userLists { get; set; }
        public List<TblRole>? rolesList { get; set; }
        public string name = "";
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                Navigate.NavigateTo("/login");
            }

            userLists = await ur.UserLists();
            rolesList = await ur.GetRoleByList();
        }

        public void onBtnUserRole()
        {
            Navigate.NavigateTo("/userRole");
        }
        public void onBtnNewuser()
        {
            Navigate.NavigateTo("/adduser");
        }
        public void onBtnNewRole()
        {
            Navigate.NavigateTo("/userRole");
        }
        public void onBtnRefresh()
        {
            Navigate.NavigateTo("/account");
        }
        public async Task DeleteUser(int id)
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់លុបមែនទេ?");
            if (result)
            {
                await ur.DeleteUser(id);
                userLists = await ur.UserLists();
                StateHasChanged();
            }
        }
    }
}
