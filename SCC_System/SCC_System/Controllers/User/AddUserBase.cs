﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.User
{
    public class AddUserBase : ComponentBase
    {
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        public UserResponse? UR { get; set; }
        public TblUser User = new TblUser();
        public List<TblEmployee> listemployee = new List<TblEmployee>();
        public List<TblRole> Roleslists = new List<TblRole>();
        public string name = "";
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }
            this.listemployee = await UR.GetEmployeeList();
            this.Roleslists = await UR.GetRoleByList();
        }

        public async Task AddNewUser()
        {
            if (User != null && UR != null)
            {
                if (!string.IsNullOrEmpty(User.Username) && !string.IsNullOrEmpty(User.Password) && User.EmpId > 0 && User.RoleId > 0)
                {
                    User.Active = true;
                    await UR.AddnewUser(User);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    await UR.GetEmployeeList();
                    await UR.GetRoleByList();
                    StateHasChanged();
                    navigation.NavigateTo("/account");
                }
            }
        }

        public void onBtnExitUser()
        {
            navigation.NavigateTo("/account");
        }
    }
}
