﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.Authentication
{

    public class AuthenticationBase : ComponentBase
    {
        [Inject]
        public AuthenResponse? auth { get; set; }
        public TblUser user = new TblUser();
        public List<TblUser>? userlists = new List<TblUser>();
        public List<TblRolePermission> role = new List<TblRolePermission>();
        [Inject]
        public UserResponse? useresponse { get; set; }
        public List<TblRole> rolelist = new List<TblRole>();
        public int rolid;
        public int perId;

        [Inject]
        NavigationManager Navigation { get; set; }
        [Inject]
        IJSRuntime Runtime { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }

        protected async override Task OnInitializedAsync()
        {
            this.userlists = await this.auth.UserLists();
            this.rolelist = await this.useresponse.GetRoleByList();
            this.role = await this.auth.GetRolePermission();
            string name = await localStorage.GetItemAsync<string>("username");

        }
        public async Task OnAuthAccess()
        {
            var username = user.Username;
            var pwd = user.Password;
            var tmp = await auth.AuthAccess(user);
            if (tmp.EmpId != 0)
            {
                await localStorage.SetItemAsync("user", tmp.UserId);
                await localStorage.SetItemAsync("username", tmp.Username);
                Navigation.NavigateTo("/");
            }
            else
            {
                await Runtime.InvokeAsync<object>("warning", "ឈ្មោះ និងពាក្យសម្ងាត់មិនត្រឹមត្រូវ!");
            }

        }
    }
}
