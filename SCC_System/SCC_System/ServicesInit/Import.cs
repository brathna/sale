﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class Import : IImport
    {
        private readonly HttpClient Client;
        public Import(HttpClient httpClient)
        {
            Client = httpClient;
        }

        public async Task<ApiResponse> CreateImport(ImportPost importPost)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Import/CreateImport", importPost);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetImportDetail(int importId)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Import/GetImportDetail/" + importId);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListImport()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Import/ListImport");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
