﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class RolePermission : IRolePermission
    {
        private readonly HttpClient Client;
        public RolePermission(HttpClient httpclient)
        {
            this.Client = httpclient;
        }

        public async Task<ApiResponse> CreateRolePermission(TblRolePermission roleper)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/RolePermission/CreateRolePermission", roleper);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> DeleteRolePermission(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/RolePermission/DeleteRolePermission/" + id);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> GetPermByList()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/RolePermission/GetPermissionList");
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> GetRolePerById(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/RolePermission/GetRolePerById/" + id);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> GetSubPermissionList()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/RolePermission/GetSubPermissionList");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListRolePermission()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/RolePermission/ListRolePermission");
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> UpdateRolePermission(TblRolePermission rolePermission)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/RolePermission/UpdateRolePermission", rolePermission);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
