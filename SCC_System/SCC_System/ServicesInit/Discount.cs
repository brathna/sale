﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class Discount : IDiscount
    {
        private readonly HttpClient Client;
        public Discount(HttpClient client)
        {
            this.Client = client;
        }

        public async Task<ApiResponse> CreateDiscount(TblDiscount cre_dis)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Discount/CreateDiscount", cre_dis);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ApiResponse> DeleteDiscount(int de_disId)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Discount/DeleteDiscount?id=" + de_disId);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> GetDiscountById(int Id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Discount/GetDiscountById?id=" + Id);
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<ApiResponse> ListDiscount()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Discount/ListDiscount");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> UpdateDiscount(TblDiscount up_dis)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Discount/UpdateDiscount", up_dis);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
