﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class ImportResponse
    {
        private readonly IImport import;
        ApiResponse response;
        public List<VwGetImportById> listimport { get; set; }

        public ImportResponse(IImport import)
        {
            this.import = import;
        }
        public async Task<List<VwGetImportById>> ImportList()
        {
            try
            {
                response = await this.import.ListImport();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        listimport = JsonConvert.DeserializeObject<List<VwGetImportById>>(Data);
                    }
                }
                return listimport;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<ApiResponse> AddImport(ImportPost imp)
        {
            try
            {
                response = await this.import.CreateImport(imp);
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public async Task EditImport()
        //{

        //}
        public async Task<ApiResponse> GetImportById(int importId)
        {
            try
            {
                response = await this.import.GetImportDetail(importId);
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
