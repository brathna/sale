﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{

    public class ResponseData
    {
        private readonly IEmployee Emp;

        public List<TblEmployee> EmployeeList { get; set; }
        public TblEmployee? Employee { get; set; }

        public ResponseData(IEmployee emp)
        {
            Emp = emp;
        }

        public async Task<List<TblEmployee>> EmployeesList()
        {
            EmployeeList = new List<TblEmployee>();
            try
            {
                ApiResponse response = await this.Emp.ListEmployee();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        EmployeeList = JsonConvert.DeserializeObject<List<TblEmployee>>(Data);
                    }
                }
                return EmployeeList;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public async Task<TblEmployee> GetEmployeeById(int id)
        {
            try
            {
                ApiResponse response = await this.Emp.GetEmployeeById(id);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        Employee = JsonConvert.DeserializeObject<TblEmployee>(Data);
                    }
                }
                return Employee;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        //====================================
        //update employee
        //====================================
        public async Task SaveEmployee(TblEmployee emp)
        {
            try
            {
                ApiResponse response = await this.Emp.UpdateEmployee(emp);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public async Task DeleteEmployee(int id)
        {
            try
            {
                ApiResponse response = await this.Emp.DeleteEmployee(id);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public async Task CreateEmployee(TblEmployee emp)
        {
            try
            {
                ApiResponse response = await this.Emp.CreateEmployee(emp);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
