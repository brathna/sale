﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;
namespace SCC_System.Services
{
    public class SaleReportResponse
    {
        private readonly ISaleReport sale;
        ApiResponse response;
        public List<sp_SaleReportResult> salereport { get; set; }
        public List<VwGetUserReport> getuserreport { get; set; }
        public SaleReportResponse(ISaleReport salereport)
        {
            this.sale = salereport;
        }

        public async Task<List<VwGetUserReport>> GetUserReport()
        {
            try
            {
                response = await this.sale.GetUserReport();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        getuserreport = JsonConvert.DeserializeObject<List<VwGetUserReport>>(Data);
                    }
                }
                return getuserreport;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<sp_SaleReportResult>> GetListSaleReport(SaleFilter filter)
        {
            try
            {
                response = await this.sale.ListSaleReport(filter);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        salereport = JsonConvert.DeserializeObject<List<sp_SaleReportResult>>(Data);
                    }
                }
                return salereport;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
