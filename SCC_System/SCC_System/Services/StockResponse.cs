﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;
namespace SCC_System.Services
{
    public class StockResponse
    {
        private readonly IStock Stock;
        ApiResponse response;
        public List<TblStock> stocks = new List<TblStock>();
        public List<VwSelectStockOut> stout = new List<VwSelectStockOut>();
        //public TblStock sto = new TblStock();
        //public TblStockOut outstock = new TblStockOut();
        public StockResponse(IStock stock)
        {
            Stock = stock;
        }
        public async Task<List<TblStock>> ListStock()
        {
            try
            {
                response = await Stock.ListStock();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        stocks = JsonConvert.DeserializeObject<List<TblStock>>(Data);
                    }
                }
                return stocks;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<List<VwSelectStockOut>> ListStouckOut()
        {
            try
            {
                response = await Stock.ListStockOut();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        stout = JsonConvert.DeserializeObject<List<VwSelectStockOut>>(Data);
                    }
                }
                return stout;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public async Task AddNewStock(TblStock stock)
        {
            try
            {
                response = await this.Stock.CreateStock(stock);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task AddNewStockOut(TblStockOut stockout)
        {
            try
            {
                response = await this.Stock.OutStock(stockout);
            }
            catch (Exception e)
            {
                throw;
            }
        }


    }
}
