using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Radzen;
using SaleModal.Services;
using SCC_System;
using SCC_System.Services;
using SCC_System.ServicesInit;
using System.Text.Json;
using System.Text.Json.Serialization;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

var API_URL = builder.Configuration.GetSection("API_URL").Value;

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(API_URL) });



//===============advertise here new class for UI =============
builder.Services.AddScoped<IEmployee, Employee>();
builder.Services.AddScoped<IProduct, Product>();
builder.Services.AddScoped<ICategory, Category>();
builder.Services.AddScoped<IUser, User>();
builder.Services.AddScoped<ISales, Sales>();
builder.Services.AddScoped<IRolePermission, RolePermission>();
builder.Services.AddScoped<ISaleReport, SaleReport>();
builder.Services.AddScoped<IExchangeRate, ExchangeRate>();
builder.Services.AddScoped<IImport, Import>();
builder.Services.AddScoped<IAuthentication, Authentication>();
builder.Services.AddScoped<IDiscount, Discount>();
builder.Services.AddScoped<IStock, Stock>();
//builder.Services.AddScoped<IServiceProvider, ServiceProvider>();



builder.Services.AddScoped<ResponseData>();
builder.Services.AddScoped<ProductResponse>();
builder.Services.AddScoped<NotificationService>();
builder.Services.AddScoped<CategoryResponse>();
builder.Services.AddScoped<UserResponse>();
builder.Services.AddScoped<RoleResponse>();
builder.Services.AddScoped<SaleResponse>();
builder.Services.AddScoped<SaleReportResponse>();
builder.Services.AddScoped<ExchangeResponse>();
builder.Services.AddScoped<ImportResponse>();
builder.Services.AddScoped<AuthenResponse>();
builder.Services.AddScoped<DiscountResponse>();
builder.Services.AddScoped<StockResponse>();

builder.Services.AddBlazoredLocalStorage(config =>
{
    config.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
    config.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
    config.JsonSerializerOptions.IgnoreReadOnlyProperties = true;
    config.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
    config.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    config.JsonSerializerOptions.ReadCommentHandling = JsonCommentHandling.Skip;
    config.JsonSerializerOptions.WriteIndented = false;
});
//builder.Services.AddBootstrapCSS();

builder.Services.AddOidcAuthentication(options =>
{
    builder.Configuration.Bind("Local", options.ProviderOptions);
});

await builder.Build().RunAsync();

